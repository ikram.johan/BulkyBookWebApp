﻿using Microsoft.AspNetCore.Mvc;
using BulkyBook.DataAccess;
using BulkyBook.Models;
using BulkyBook.DataAccess.Repository.IRepository;

namespace BulkyBookWeb.Controllers
{
    [Area("Admin")]
    public class CoverTypeController : Controller
    {
        readonly IUnitOfWork _unitOfWOrk;

        public CoverTypeController(IUnitOfWork unitOfWork)
        {
            _unitOfWOrk = unitOfWork;
        }
        public IActionResult Index()
        {
            IEnumerable<CoverType> coverTypes = _unitOfWOrk.CoverType.GetAll();
            return View(coverTypes);
        }

        //GET
        public IActionResult Create()
        {

            return View();
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CoverType coverType)
        {
            if (ModelState.IsValid)
            {
                _unitOfWOrk.CoverType.Add(coverType);
                _unitOfWOrk.Save();
                TempData["success"] = "Category created successfully";
                return RedirectToAction("Index");
            }
            return View(coverType);
        }
        //GET
        public IActionResult Edit(int? id)
        {
            if (id==0 || id==null)
            {
                return NotFound();
            }
            var coverType= _unitOfWOrk.CoverType.GetFirstOrDefault(c=>c.Id==id);
            if (coverType == null)
            {
                return NotFound();
            }

            return View(coverType);
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(CoverType coverType)
        {
            if (ModelState.IsValid)
            {

                _unitOfWOrk.CoverType.Update(coverType);
                _unitOfWOrk.Save();
                TempData["success"] = "Category updated successfully";
                return RedirectToAction("Index");
            }
            return View(coverType);
        }
        //GET
        public IActionResult Delete(int? id)
        {

            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var coverType = _unitOfWOrk.CoverType.GetFirstOrDefault(c => c.Id == id);
            if (coverType == null)
            {
                return NotFound();
            }

            return View(coverType);
        }

        //POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePost(int? id)
        {
            var coverType = _unitOfWOrk.CoverType.GetFirstOrDefault(c => c.Id == id);
            if (coverType==null)
            {
                return NotFound();
            }
            _unitOfWOrk.CoverType.Remove(coverType);
            _unitOfWOrk.Save();
            TempData["success"] = "Category deleted successfully";
            return RedirectToAction("Index");
        }
    }
}
