﻿using Microsoft.AspNetCore.Mvc;
using BulkyBook.DataAccess;
using BulkyBook.Models;
using BulkyBook.DataAccess.Repository.IRepository;
using Microsoft.AspNetCore.Mvc.Rendering;
using BulkyBook.Models.ViewModel;
using Microsoft.AspNetCore.Hosting;

namespace BulkyBookWeb.Controllers
{
    [Area("Admin")]
    public class ProductController : Controller
    {
        readonly IUnitOfWork _unitOfWOrk;
        readonly IWebHostEnvironment _webHostEnvironment;

        public ProductController(IUnitOfWork unitOfWork, IWebHostEnvironment webHostEnvironment)
        {
            _unitOfWOrk = unitOfWork;
            _webHostEnvironment = webHostEnvironment;
        }
        public IActionResult Index()
        {
          //  IEnumerable<CoverType> coverTypes = _unitOfWOrk.CoverType.GetAll();
            return View();
        }

        //GET
        public IActionResult Upsert(int? id)
        {
            ProductVM productVM = new ProductVM()
            {
                Product = new(),
                CategoryList = _unitOfWOrk.Category.GetAll().Select(u => new SelectListItem
                {
                    Text = u.Name,
                    Value = u.Id.ToString()
                }),
                CoverTypeList = _unitOfWOrk.CoverType.GetAll().Select(u => new SelectListItem
                {
                    Text = u.Name,
                    Value = u.Id.ToString()
                }),
            };

            if (id == 0 || id == null)
            {
                // ViewBag.CategoryList=CategoryList;// Use of Viewbag
                // ViewData["CoverTypeList"] = CoverTypeList;
                //Create();
                return View(productVM);
            }
            else
            {
                productVM.Product = _unitOfWOrk.Product.GetFirstOrDefault(u=>u.Id==id);
                return View(productVM);
                //Update();
            }


           
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpSert(ProductVM productVM, IFormFile? file)
        {
            if (ModelState.IsValid)
            {
                string wwwRootPath = _webHostEnvironment.WebRootPath;
                if (file!=null)
                {                 
                    string fileName=Guid.NewGuid().ToString();
                    string upload=Path.Combine(wwwRootPath, @"images\products");
                    string extenstion=Path.GetExtension(file.FileName);
                    if (productVM.Product.ImageUrl!=null)
                    {
                        var oldImagePath = Path.Combine(wwwRootPath, productVM.Product.ImageUrl.Trim('\\'));
                        if (System.IO.File.Exists(oldImagePath))
                        {
                            System.IO.File.Delete(oldImagePath);
                        }
                    }
                    using (var fileStreams = new FileStream(Path.Combine(upload, fileName + extenstion), FileMode.Create))
                    {
                        file.CopyTo(fileStreams);
                    }
                    productVM.Product.ImageUrl = @"\images\products\" + fileName + extenstion;
                }
                if (productVM.Product.Id==0)
                {
                    _unitOfWOrk.Product.Add(productVM.Product);
                   
                }
                else
                {
                    _unitOfWOrk.Product.Update(productVM.Product);
                    //TempData["success"] = "Category updated successfully";
                }
                _unitOfWOrk.Save();
                TempData["success"] = "Category created successfully";
                return RedirectToAction("Index");
            }
            return View(productVM);
        }
        //GET
      /*  public IActionResult Delete(int? id)
        {

            if (id == 0 || id == null)
            {
                return NotFound();
            }
            var coverType = _unitOfWOrk.CoverType.GetFirstOrDefault(c => c.Id == id);
            if (coverType == null)
            {
                return NotFound();
            }

            return View(coverType);
        }

        //POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePost(int? id)
        {
            var coverType = _unitOfWOrk.CoverType.GetFirstOrDefault(c => c.Id == id);
            if (coverType == null)
            {
                return NotFound();
            }
            _unitOfWOrk.CoverType.Remove(coverType);
            _unitOfWOrk.Save();
            TempData["success"] = "Category deleted successfully";
            return RedirectToAction("Index");
        }
      */

        #region API CALLS
        [HttpGet]
        public IActionResult GetAll()
        {
            var productList = _unitOfWOrk.Product.GetAll(includeProperties:"Category,CoverType");
            return Json(new {data=productList});
        }

        [HttpDelete]
        public IActionResult Delete(int? id)
        {
            var product = _unitOfWOrk.Product.GetFirstOrDefault(c => c.Id == id);
            if (product == null)
            {
                return Json(new {success=false,message="Error while deleting"});
            }
            var oldImagePath = Path.Combine(_webHostEnvironment.WebRootPath, product.ImageUrl.Trim('\\'));
            if (System.IO.File.Exists(oldImagePath))
            {
                System.IO.File.Delete(oldImagePath);
            }
            _unitOfWOrk.Product.Remove(product);
            _unitOfWOrk.Save();
            // TempData["success"] = "Category deleted successfully";
            return Json(new { success = true, message = "Deleted successfully" });
        }

        #endregion
    }
}
